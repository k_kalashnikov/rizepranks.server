﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using RizePranks.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RizePranks.Core
{
    public class Program
    {
        public static void Main(string[] args)
        {
        }

        public class MigrationsContextFactory : IDbContextFactory<ApplicationDbContext>
        {
            public ApplicationDbContext Create(DbContextFactoryOptions options)
            {
                var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
                builder.UseSqlServer("Server=VIKI\\SQLEXPRESS;Database=rizepranks;Integrated Security=yes;Trusted_Connection=True;MultipleActiveResultSets=true;", b => b.MigrationsAssembly("RizePranks.Server"));
                return new ApplicationDbContext(builder.Options);
            }
        }
    }
}
