﻿using RizePranks.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RizePranks.Core.Models
{
    public class Image : BaseModel<int>
    {
        public ImageType Type { get; set; }
    }
}
