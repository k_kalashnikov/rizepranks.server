﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RizePranks.Core.Models
{
    public class Toy : BaseModel<int>
    {
        public virtual ICollection<Image> Images { get; set; }
    }
}
