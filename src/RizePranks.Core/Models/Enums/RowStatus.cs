﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RizePranks.Core.Models.Enums
{
    public enum RowStatus
    {
        Active = 0,
        NotActive = 1,
        Incomplite = 2
    }
}
