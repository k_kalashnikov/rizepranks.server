﻿using RizePranks.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RizePranks.Core.Models
{
    public class BaseModel<TPK>
    {
        public TPK Id { get; set; }
        public string Name { get; set; }
        public RowStatus Status { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime CreateFrom { get; set; }
    }
}
