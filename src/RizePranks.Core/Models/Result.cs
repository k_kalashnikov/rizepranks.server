﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RizePranks.Core.Models
{
    public class Result<TModel> where TModel : class
    {
        public string Message { get; set; }
        public TModel Object { get; set; }

        public Result(TModel _Object, string _Message = "")
        {
            Message = _Message;
            Object = _Object;
        }

    }
}
