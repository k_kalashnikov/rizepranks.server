﻿using RizePranks.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RizePranks.Core.Repository
{
    public class UnitOfWorks : IDisposable
    {
        protected ApplicationDbContext context { get; private set; }

        public UnitOfWorks(ApplicationDbContext _context)
        {
            context = _context;
            var properties = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
            {
                var instance = Activator.CreateInstance(property.PropertyType, this.context);
                property.SetValue(this, instance, null);
            }
        }

        public int Commit()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
