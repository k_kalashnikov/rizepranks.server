﻿using Microsoft.EntityFrameworkCore;
using RizePranks.Core.Models;
using RizePranks.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RizePranks.Core.Repository
{
    public class BaseRepository<TModel, TPK> where TModel : BaseModel<TPK>
    {
        protected DbContext context { get; set; }
        protected DbSet<TModel> entitySet { get; set; }

        public BaseRepository(DbContext _context)
        {
            if (_context == null)
            {
                throw new ArgumentNullException(nameof(_context), "Контекст базы данных не передан или равен NULL");
            }

            context = _context;
            entitySet = context.Set<TModel>();
        }
        public virtual Result<TModel> Add(TModel item)
        {
            try
            {
                TModel result = entitySet.Add(item).Entity;
                return new Result<TModel>(result, $"Элемент {item.Name} успешно добавлен");
            }
            catch (Exception e)
            {
                return new Result<TModel>(item, e.Message);
            }
        }
        public virtual Result<TModel> Delete(TModel item)
        {
            try
            {
                entitySet.Remove(item);
                return new Result<TModel>(item, $"Элемент {item.Name}  успешно удалён");
            }
            catch(Exception e)
            {
                return new Result<TModel>(item, e.Message);
            }
        }
        public virtual Result<TModel> DeleteById(TPK id)
        {
            TModel item = GetById(id);
            if (item == null)
            {
                return new Result<TModel>(null, $"Удаление элемента с ID = {id} прошло с ошибкой. Элемент c ID = {id} не найден");
            }
            return Delete(item);
        }
        public virtual TModel GetById(TPK id)
        {
            return GetAll().FirstOrDefault(m => m.Id.Equals(id));
        }
        public virtual IQueryable<TModel> GetAll()
        {
            return entitySet;
        }
        public virtual IQueryable<TModel> GetAll(List<RowStatus> withStatuses)
        {
            withStatuses = withStatuses ?? new List<RowStatus>() { RowStatus.Active, RowStatus.Incomplite};
            return GetAll().Where(m => withStatuses.Contains(m.Status));
        }
        public virtual IQueryable<TModel> GetAll(Expression<Func<TModel, TPK>> orderBy)
        {
            return GetAll().OrderBy(orderBy);
        }
        public virtual IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate)
        {
            return GetAll().Where(predicate);
        }
        public virtual IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate, List<RowStatus> withStatuses)
        {
            return GetAll(withStatuses).Where(predicate);
        }
        public Result<TModel> Update(TModel item)
        {
            try
            {
                TModel result = entitySet.Update(item).Entity;
                return new Result<TModel>(result, $"Элемент {item.Name} успешно обновлен");
            }
            catch (Exception e)
            {
                return new Result<TModel>(item, e.Message);
            }
        }
        public Result<TModel> Аctivate(TModel item)
        {
            item.Status = (item.Status == RowStatus.Active) ? RowStatus.NotActive : RowStatus.Active;
            return Update(item);
        }
        public Result<TModel> АctivateById(TPK id)
        {
            TModel item = GetById(id);
            if (item == null)
            {
                return new Result<TModel>(null, $"Изменение элемента с ID = {id} прошло с ошибкой. Элемент c ID = {id} не найден");
            }
            return Аctivate(item);
        }
    }
}
