﻿window.delayedFunction = new Object;

function getOurHeightMax(elList) {
    var maxH = -1;
    $(elList).each(function () {
        var curH = parseInt($(this).height());
        maxH = (curH > maxH) ? curH : maxH;
    });
    console.log(maxH);
    $(elList).each(function () {
        $(this).height(maxH);
    });
}

function getOurHeightMin(elList) {
    var maxH = $(elList).first().height();
    $(elList).each(function () {
        var curH = parseInt($(this).height());
        maxH = (curH < maxH) ? curH : maxH;
    });
    console.log(maxH);
    $(elList).each(function () {
        $(this).height(maxH);
    });
}

function TableObject(element) {
    var self = this;
    self.obj = $(element);

    self.Init = function () {
        self.table = self.obj.DataTable({
            stateSave: true,
            responsive: true,
            language: {
                "processing": "Подождите...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                },
                "aria": {
                    "sortAscending": ": активировать для сортировки столбца по возрастанию",
                    "sortDescending": ": активировать для сортировки столбца по убыванию"
                }
            }
        });

        $('.delete').click(function () {
            if (!confirm("Вы действительно хотите удалить элемент?")) {
                return false;
            }
            var dataPost = $(this).attr('data-post');
            var url = $(this).attr('href');
            var token = $('input[name="__RequestVerificationToken"]').val();
            var data =
            {
                id: dataPost,
                __RequestVerificationToken: token
            };
            $.post(url, data, function () {
                window.location.reload();
            });
            return false;
        });
    }

    self.Init();
}

$(window).load(function () {
    $('.preloader').hide();
    for (var item in window.delayedFunction) {
        window.delayedFunction[item]();
    }
});