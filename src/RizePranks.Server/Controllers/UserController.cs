﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using RizePranks.Server.Services;
using Microsoft.AspNetCore.Identity;
using RizePranks.Core.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace RizePranks.Server.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private UserManager<ApplicationUser> userManager { get; set; }
        private SignInManager<ApplicationUser> signInManager { get; set; }
        private IEmailSender emailSender { get; set; }
        private ISmsSender smsSender { get; set; }
        private ILogger logger { get; set; }


        public UserController(UserManager<ApplicationUser> _userManager, SignInManager<ApplicationUser> _signInManager, IEmailSender _emailSender, ISmsSender _smsSender, ILoggerFactory _loggerFactory)
        {
            userManager = _userManager;
            signInManager = _signInManager;
            emailSender = _emailSender;
            smsSender = _smsSender;
            logger = _loggerFactory.CreateLogger<UserController>();
        }
        public IActionResult Index()
        {
            var result = userManager.Users.ToList();
            return View(result);
        }
    }
}
